//
//  BlockchainApi.swift
//  BitcoinInvest
//
//  Created by Maxim Shmotin on 02/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

public class BlockchainApi: ApiService {
    private let API_SINGLE_ADDRESS: NSString = "https://blockchain.info/ru/rawaddr/"
    private let API_LAST_BITCOIN_PRICE = "https://blockchain.info/ru/ticker"
    private let JSON: NSString = "?format=json"
    
    override init () {
        super.init()
    }
    
    public func getLastBitcoinPrice()  -> NSData {
        return getResponseFromPublicServerUrl(API_LAST_BITCOIN_PRICE)
    }
    
    public func getDataForWalletAddress(address: NSString) -> NSData {
        return getResponseFromPublicServerUrl("\(API_SINGLE_ADDRESS)\(address)\(JSON)")
    }
    
   
}
