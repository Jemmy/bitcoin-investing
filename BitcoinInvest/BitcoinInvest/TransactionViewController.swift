//
//  TransactionViewController.swift
//  BitcoinInvest
//
//  Created by Maxim Shmotin on 31/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class TransactionViewController: UITableViewController, UITableViewDelegate, UITableViewDataSource {
    private let cellId = "TransactionCell"
    var transactions: [Transaction]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    // Table View
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.transactions.count
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellId, forIndexPath: indexPath) as UITableViewCell
        cell.textLabel?.text = transactions[0].fullInfo as String
        return cell
    }


}
